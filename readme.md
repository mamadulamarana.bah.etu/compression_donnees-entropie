## Auteurs:
BAH Mamadu Lamarana
## Reponses aux questions :

### Q1.1
L'entropie d'une source est maximale, c'est-à-dire égale à log2(N),(avec N taille en nombre d'octet du fichier) si et seulement si la distribution
de probabilité est uniforme.
L'entropie maximale est 8.
Cela est atteint quand le nombre d'occurence de chaque symbole est identique : probabilité de chaque symbole est équiprobable.


### Q1.2
voir l'image Q1.2.jpg.

### Q2.7
Au mieux, la taille d'un fichier codé avec un codage optimal est N*H(s) avec N taille en nombre d'octet du fichier.

## Experiences

### adresses des files
image => https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/2018_-_Nyhavn_on_sunset.jpg/750px-2018_-_Nyhavn_on_sunset.jpg

audio=> https://upload.wikimedia.org/wikipedia/commons/a/a9/A_Carol_Brynging_in_the_Bore%27s_Heed.ogg

code Source => 
https://gitlab.com/mica-automated-pumping-system/automated-temperature-monitoring/-/raw/main/alarm-framework/ADCService.h?inline=false

fichier Zip => https://gitlab.com/jkushmaul/rust_msvc/-/archive/master/rust_msvc-master.zip?path=src

### Q3.1
| Nom | Taille(octets) | Entropie |
| ------ | ------ | ------ |
| audio.ogg         |2864421| 7.59 |
| image.jpg         | 91360 | 8    |
| fichier_zip.zip   | 3325  | 7.72 |
| tp_entropy.zip    | 4153  | 7.48 |  
| codeSource        | 888   | 4.80 |

### Q3.2
Nous remarquons que les fichiers medias jpg et mp3 ont une entropie presque egale et qu'elle est plus grande que celle des fichiers compressés zip et codes sources.
L'entropie d'un fichier code source est la plus faible.
L'entropie d'un fichier image jpg et est la plus grande.
Toutes ces differences sont dues à la maniere dont sont codés ces differents types de fichiers.