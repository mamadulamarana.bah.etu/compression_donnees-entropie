#pragma once

// CircuitPlayground CPLAY_LIGHTSENSOR

#include <Streaming.h>
#include <Every.h>

class ADCService : public ServiceWithValue {
    // provide a value from an ADC pin
  public:
    const int pin;
    
    Every sample_time = Every(1000/10);
    Every print_time = Every(1000*10);

    ADCService(int pin) : pin(pin) {
    }

    const char * const description() const {
      return "Bare ADC"; // FIXME: ha, want the pin# here
    }

    void begin() {
      pinMode( pin, INPUT );
      status = ServiceInterface::Statuses::OK;
      Serial << F("ADCB ") << pin << endl;
    }

    void run() {
      if ( sample_time() ) {
        value = analogRead( pin );
        if ( print_time() ) { // FIXME: should be a debug thingy
          Serial << F("ADC ") << pin << F(" = ") << value << F(" ") << (3 * (value / float(1<<12)) ) << F("V") << endl;
        }
      }
    }

};
