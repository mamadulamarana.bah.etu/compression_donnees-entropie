#include<stdio.h>
#include<stdlib.h>

#include "lib/entropy.h"

int main(int argc, char **argv) {

  if (argc <= 1) {
    fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
    exit(1);
  }
  FILE *file = fopen(argv[1], "r");
  int counts[256];
  int i;
  for (i = 0; i < 256; i++) {
    counts[i] = 0;
  }

  count_occurrences(file, counts);
  struct file_stat entrop = entropy(counts);
  float percent = (1.0 - (entrop.entropy /8))*100;
  printf("le fichier %s fait  %d octets et a une entropie de %.2f par octet \n", argv[1], entrop.size, entrop.entropy);
  printf("Au mieux un codage optimal améliorerait le stockage de ce fichier de %.0f% \n", percent);

  fclose(file);
  exit(0);
}
